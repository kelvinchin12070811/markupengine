#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <sstream>
#include <boost/algorithm/clamp.hpp>
#include <rapidjson/istreamwrapper.h>
#include "MarkupEngine/YAMLEngine.hpp"
#include "MarkupEngine/XMLEngine.hpp"
#include "MarkupEngine/JSONEngine.hpp"

using namespace std;

void yaml_test()
{
	mkengine::YAMLEngine engine;
	engine.loadFile("config test.yaml");
	cout << boolalpha;
	string test = engine.getEntry<string>("config.native_fullscreen");

	cout << test << endl;
	cout << engine.getEntry<string>("config.magic") << endl;
	cout << engine.getEntry<string>("config.canvas_size.0") << endl;
	cout << engine.getEntry<string>("config.canvas_size.1") << endl;

	cout << endl << endl;

	cout << engine.getEntry<string>("config.messages.0") << endl;
	cout << engine.getEntry<string>("config.messages.1") << endl;

	engine.getNode("config.messages").push_back("the new message inserted");

	/*engine.getNode("config").force_insert("vas_name", YAML::Node(YAML::NodeType::Null));
	engine.getNode("config.vas_name") = "sakura miyamoto";*/

	if (!engine.getNode("config.debug").IsDefined())
		engine.insert("config", "debug");

	engine.insert("config.debug", "vas_name", "sakura miyamoto");
	cout << endl << endl;
	cout << engine.dump() << endl;
}

void xml_test()
{
	mkengine::XMLEngine engine;
	engine.loadFile("config xml test.xml");
	cout << engine->child("config").child("messages").first_child().attribute("text").as_string() << endl;

	engine->child("config").child("messages").first_child().attribute("text") = "new string";

	cout << engine.dump() << endl;

	cout << endl << endl;
	//auto test = engine->select_node("/config/magic").attribute();
	pugi::xml_attribute node;
	node.set_name("test_name");
	node.set_value("test string");
	cout << engine.getNode("config.messages.message.text").attribute().set_value("test string") << endl;
	engine.getNode("config").node().append_child("text");
	engine.getNode("config.text").node().text().set("new test string");
	//engine.getNode("config").node().remove_child("text");
	//cout << varSet. << endl;

	cout << engine.dump() << endl;

	cout << endl;

	auto test = engine.getNodes("config.messages.message");
	cout << test.size() << endl;
	auto itr = test.begin();
	std::advance(itr, boost::algorithm::clamp(0, 0, test.size() - 1));
	itr->node().print(cout);
}

void json_test()
{
	mkengine::JSONEngine engine;
	engine.loadFile("config json test.json");
	if (engine->HasMember("config"))
	{
		if ((*engine)["config"].HasMember("magic"))
		{
			cout << (*engine)["config"]["magic"].GetString() << endl;
		}
	}
	auto magicNum = engine.getNode("config.magic");
	if (magicNum != nullptr)
		cout << magicNum->GetString() << endl;

	auto message1 = engine.getNode("config.messages.0");
	if (message1)
		cout << message1->GetString() << endl;
	cout << engine.getNode("config.messages.1", rapidjson::Value("")).GetString() << endl;

	cout << engine.dump() << endl;
}

int main()
{
	cout << boolalpha;
	try
	{
		xml_test();
	}
	catch (const std::exception& e)
	{
		cout << e.what() << endl;
	}

	system("pause");
	return 0;
}