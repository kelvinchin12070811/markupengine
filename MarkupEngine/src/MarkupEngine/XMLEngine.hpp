#pragma once
#include <memory>
#include <pugixml/pugixml.hpp>
#include <iostream>
#include <vector>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <boost/algorithm/string.hpp>

namespace mkengine
{
	template <typename StringType = std::string>
	class XMLEngineTemplate
	{
	public:
		enum class PathType : uint8_t { string_path, xpath };

	public:
		XMLEngineTemplate() :
			document(std::make_unique<pugi::xml_document>())
		{
		}

		XMLEngineTemplate(XMLEngineTemplate && rhs) :
			document(std::move(rhs.document))
		{
		}

		~XMLEngineTemplate()
		{
		}

		void loadFile(const StringType & file, uint32_t option = pugi::parse_default | pugi::parse_declaration)
		{
			auto result = document->load_file(file.c_str(), option);
			if (result.status != pugi::xml_parse_status::status_ok)
				throw std::runtime_error(result.description());
		}

		void loadFile(std::ifstream & inputFile, uint32_t option = pugi::parse_default | pugi::parse_declaration)
		{
			auto result = document->load(inputFile, option);
			if (result.status != pugi::xml_parse_status::status_ok)
				throw std::runtime_error(result.description());
		}

		void loadString(const StringType & buffer, uint32_t option = pugi::parse_default | pugi::parse_declaration)
		{
			auto result = document->load_string(buffer.c_str(), option);
			if (result.status != pugi::xml_parse_status::status_ok)
				throw std::runtime_error(result.description());
		}

		StringType dump() const
		{
			std::stringstream ss;
			document->save(ss);
			return ss.str();
		}

		void dump(std::ofstream & output) const
		{
			document->save(output);
		}

		void dump(const StringType & filePath) const
		{
			std::ofstream outputFile;
			outputFile.exceptions(std::ofstream::failbit | std::ofstream::badbit);
			outputFile.open(filePath);
			dump(outputFile);
			outputFile.close();
		}

		XMLEngineTemplate & operator=(XMLEngineTemplate && rhs)
		{
			this->document = std::move(rhs.document);
			return *this;
		}

		pugi::xml_node getNode()
		{
			return document->first_child();
		}

		pugi::xpath_node getNode(const StringType & path, PathType type = PathType::string_path, pugi::xml_node * root = nullptr)
		{
			StringType xpath = type == PathType::xpath ? path : stringPath2XPath(path);
			if (root != nullptr)
				return root->select_node(xpath.c_str());
			else
				return document->select_node(xpath.c_str());
		}

		pugi::xpath_node_set getNodes(const StringType & path, PathType type = PathType::string_path, pugi::xml_node * root = nullptr)
		{
			StringType xpath = type == PathType::xpath ? path : stringPath2XPath(path);

			if (root != nullptr)
				return root->select_nodes(xpath.c_str());
			else
				return document->select_nodes(xpath.c_str());
		}

		const pugi::xml_node getNode() const
		{
			return document->first_child();
		}

		pugi::xml_document * operator->()
		{
			return document.operator->();
		}

		const pugi::xml_document * operator->() const
		{
			return document.operator->();
		}

		operator std::unique_ptr<pugi::xml_document>&()
		{
			return document;
		}

		operator const std::unique_ptr<pugi::xml_document>&() const
		{
			return document;
		}

	private:
		StringType stringPath2XPath(const StringType & stringPath)
		{
			StringType xpath = "/";
			std::vector<StringType> pathToken;
			boost::algorithm::split(pathToken, stringPath, [](char value) {return value == '.'; });
			xpath += boost::algorithm::join(pathToken, "/");
			return xpath;
		}
	private:
		std::unique_ptr<pugi::xml_document> document{ nullptr };
	};
	using XMLEngine = XMLEngineTemplate<>;
}